﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace TestSurfacePlot
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            var hehe = new List<int[]> {
                new int[4]  { 24499000,2,5,10000000} ,
                new int[4] { 2,1000,10000000,3} ,
                new int[4]  { 5,2,100000000,0 },
                new int[4] {0 ,3 ,3,5} };
            for (int i = 0; i < 3; i++)
            {
                hehe.Add(hehe[i]);
            }
            surfaceRail1LeftSignalAmplitude.AmplitudeOf4Frequency = hehe;
        }
    }
}
