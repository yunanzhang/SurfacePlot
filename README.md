# SurfacePlot

#### 介绍
该项目使用GitHub上的开源项目HelixToolkit库，开发的三维贝塞尔曲面，可以显示两个维度下的幅度值。以下是介绍的链接https://blog.csdn.net/yunan_zhang/article/details/79408391

#### 软件架构
软件架构说明

#### 开源支持
![微  信](https://foruda.gitee.com/images/1735525516939699668/461739fd_1763552.jpeg "微信.jpg")
![支付宝](https://foruda.gitee.com/images/1735525538614068269/fc32c9fe_1763552.jpeg "支付宝.jpg")

#### 安装教程

1.  xxxx
2.  xxxx
3.  xxxx

#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
